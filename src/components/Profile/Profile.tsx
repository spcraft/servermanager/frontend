import * as React from "react";
import * as profile from "../../store/Profile";
import { connect } from "react-redux";
import { ApplicationState } from "../../store";

interface PropMethods {
}


declare type Props = profile.State & PropMethods;


class Profile extends React.Component<Props> {
    render() {
        return ([
            <div className="Profile">
                <div className="table-row">
                    <div className="table-col table-key">
                        Username
                    </div>
                    <div className="table-col table-val">
                        { this.props.uid }
                    </div>
                </div>
                <div className="table-row table-key">
                    <div className="table-col">
                        Full name
                    </div>
                    <div className="table-col table-val">
                        { this.props.cn }
                    </div>
                </div>
                <div className="table-row table-key">
                    <div className="table-col">
                        Email
                    </div>
                    <div className="table-col table-val">
                        { this.props.mail }
                    </div>
                </div>
                <div className="table-row table-key">
                    <div className="table-col">
                        Login shell
                    </div>
                    <div className="table-col table-val">
                        { this.props.loginShell }
                    </div>
                </div>
                { 
                    this.props.isLoggedIn ? 
                    (<a id="button-logout" href="/login"> Logout </a>) :
                    "" 
                }
            </div>
        ]);
    }
}


export default connect(
    (state: ApplicationState): profile.State => ({ ...state.profile }),
    (dispatch: Function): PropMethods => ({})
)(Profile);
import * as React from "react";
import Timeline from "../Timeline";
import Profile from "../Profile";
import Hosts from "../HostList";
import FocusPopup from "../FocusPopup";

export interface Props {
    children?: React.ReactNode
}

export interface State {
}

export default class App extends React.Component<Props, State> {
    render() {
        return (
            <div className="App">
                <header>
                    <div className="first-word">SPC</div>
                    <div className="second-word">ServerManager</div>
                </header>

                <main>
                    <Timeline/>
                    <div className="right-side">
                        <Profile/>
                        <Hosts/>
                    </div>
                    <FocusPopup />
                </main>

                <footer>
                    &#9400; SteampunkCrafting team 2019
                </footer>
            </div>
        )
    }
}

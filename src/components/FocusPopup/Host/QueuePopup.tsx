import React from "react";
import doJsonRequest, { JsonResponse } from "../../../util/doJsonRequest";
import * as host from "../../../store/Host";
import * as hostList from "../../../store/HostList";
import { connect } from "react-redux";
import store, { ApplicationState, IdObject } from "../../../store";
import Decoration from "../../Decoration";
import { form, datetimeInput, checkboxInput, scanForm } from "../FormGenerators";


class QueuePopup extends React.Component<Props & Methods, State> {
    constructor(props: Props & Methods) {
        super(props);
        this.state = {
            cancellationSet: new Set<string>(),
            isLoading      : false,
            startDate      : new Date(),
            endDate        : undefined,
        }
    }

    render() {
        return (
            <div>
                <h4>{ this.props.name }</h4>
                <h5>Creating a queue task</h5>
                {
                    form("queue_task_creation", [
                        datetimeInput(
                            "Start Date:", 
                            this.state.startDate,
                            (date) => this.setState({
                                startDate: new Date(date.toString())
                            })
                        ),
                        checkboxInput(
                            "Is Endless:", 
                            "", 
                            true, 
                            (val) => this.setState({ 
                                endDate: val? undefined : new Date()
                            })
                        ),
                        ...this.state.endDate ? [
                            datetimeInput(
                                "EndDate:",
                                this.state.endDate,
                                (date) => this.setState({
                                    endDate: new Date(date.toString())
                                })
                            )
                        ]:[]
                    ])
                }
                {
                    this.state.isLoading ? 
                    <Decoration type="LDS_ELLIPSIS_LOADER" /> :
                    <button onClick={() => {
                        if(!this.props.userQueue) return;
                        this.setState({ isLoading: true });
                        doJsonRequest(`/task`, "POST", {
                            template: this.props.userQueue.templateId,
                            startDate: this.state.startDate,
                            endDate: this.state.endDate ? 
                                this.state.endDate.toISOString() : 
                                undefined
                        }).then(res => {
                            alert(res.statusCode === 200 ?
                                "Successfully created Queue task" :
                                "Failed to create Queue task"
                            );
                            this.setState({ isLoading: false });
                            return store.dispatch(hostList.fetchHost(this.props));
                        }).catch((res: JsonResponse<{ message: string, content: string }>) => {
                            this.setState({ isLoading: false });
                            alert(res.body.content);
                        });
                    }}> Create Task </button>
                }
                <hr/>
                <h5>Current queue tasks</h5> 
                <ul>{ 
                    this.props.userQueue && 
                    this.props.userQueue.myActiveQueueTasks.length > 0 ? [
                        ...this.props.userQueue.myActiveQueueTasks.map(task =>
                            <li>
                                From: { task.startDate.toUTCString() }
                                <br/>
                                To: { 
                                    task.endDate ? 
                                    task.endDate.toUTCString() : 
                                    "FOREVER" 
                                }
                                <br/>
                                Status: { task.status }
                                <br/>
                                {
                                    this.state.cancellationSet.has(task._id) ?
                                    <Decoration type="LDS_ELLIPSIS_LOADER" /> :
                                    <button
                                    className="interface-button" 
                                    onClick={() => {
                                        this.setState({ 
                                            cancellationSet: new Set(
                                                this.state.cancellationSet.add(
                                                    task._id
                                                )
                                            ) 
                                        });
                                        doJsonRequest<{ message: string }>(
                                            `/task/${ task._id }/cancel`,
                                            "GET"
                                        ).then(res => setTimeout(() => 
                                            alert(res.body.message))
                                        ).then(() => store.dispatch(
                                            hostList.fetchHost(
                                                this.props._id
                                            )
                                        ))
                                    }}
                                    >X</button>
                                }
                            </li>
                        )
                    ] : [ <tr><td> There are no active queue tasks right now. </td></tr> ]
                }</ul>    
            </div>
        );
    }
}


type Props = host.State;
interface State {
    isLoading: boolean;
    cancellationSet: Set<string>;
    startDate: Date,
    endDate?: Date
}
interface Methods {

}

export default connect(
    (state: ApplicationState, ownProps: IdObject): Props => ({
        ...state.hostList[ownProps._id],
        ...state.focusPopup
    }),
    (dispatch: Function, ownProps: IdObject): Methods => ({})
)(QueuePopup);
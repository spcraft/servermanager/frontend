import * as host from "../../../store/Host";
import React from "react";
import { connect } from "react-redux";
import { ApplicationState, IdObject } from "../../../store";
import * as focusPopup from "../../../store/FocusPopup";
import * as hostList from "../../../store/HostList";
import Decoration from "../../Decoration";
import Popup from "reactjs-popup";
import QueuePopup from "./QueuePopup";


class ViewPopup extends React.Component<Props & Methods, State> {
    constructor(props: Props & Methods) {
        super(props);
        this.state = {
            isQueuePopupOpen: false,
            isConfirmingDelete: false,
            timeoutId: NaN,
        }
    }

    componentWillUnmount() {
        window.clearTimeout(this.state.timeoutId);
    }

    deleteSelf() {
        if(this.state.isConfirmingDelete) {
            this.props.deleteSelf();
        } else {
            this.setState({
                timeoutId: window.setTimeout(
                    () => this.setState({ isConfirmingDelete: false }), 3000
                ),
                isConfirmingDelete: true
            });
        }
    }

    render() {
        return ([
            <table><tbody>{[
                <tr>
                    <td> Hostname: </td>
                    <td> { this.props.name } </td>
                </tr>,
                <tr>
                    <td> Address: </td>
                    <td> { this.props.address } </td>
                </tr>,
                <tr>
                    <td> Status: </td>
                    <td> { this.props.isOnline ? "Online" : "Offline" } </td>
                </tr>,
                ...this.props.userQueue ? [
                    <tr>
                        <td> Users Online: </td>
                        <td>{ this.props.userQueue.usersOnline.length }</td>
                    </tr>,
                    <tr>
                        <td> Your Queue status: </td>
                        <td> { 
                            this.props
                            .userQueue.usersOnline
                            .includes(this.props.uid) ?
                            "IN" : "NOT IN"
                        } </td>
                    </tr>
                ]:[],
            ]}</tbody></table>,
            ...this.props.isLoading ? [ <Decoration type="LDS_ELLIPSIS_LOADER" /> ] : [
                <button 
                    className="interface-button button-edit" 
                    onClick={ this.props.startEdit }
                />,
                <button 
                    className={ 
                        `interface-button button-delete${
                            this.state.isConfirmingDelete ? "-confirm" : ""
                        }`
                    }
                    onClick={ this.deleteSelf.bind(this) }
                />,
                ...this.props.userQueue ? [
                    <button 
                        className={ `interface-button button-queue` }
                        onClick={ () => this.setState({ isQueuePopupOpen: true }) }
                    />,
                    <Popup 
                        onClose={ () => this.setState({ isQueuePopupOpen: false }) }
                        open={ this.state.isQueuePopupOpen }
                        children = {<QueuePopup _id={ this.props._id } />}
                    />
                ] : [],
            ]
        ]);
    }
}

interface State {
    isConfirmingDelete: boolean;
    timeoutId: number;
    isQueuePopupOpen: boolean;
}

type Props = host.State & { uid: string } & focusPopup.State;
interface Methods {
    startEdit: () => void
    deleteSelf: () => void
    wakeonlanSelf: () => void
};
export default connect(
    (state: ApplicationState, ownProps: IdObject): Props => ({
        uid: state.profile.uid,
        ...state.hostList[ownProps._id],
        ...state.focusPopup
    }),
    (dispatch: Function, ownProps: IdObject): Methods => ({
        startEdit: () => dispatch(focusPopup.startEdit()),
        deleteSelf: () => dispatch(hostList.deleteHost(ownProps._id)),
        wakeonlanSelf: () => dispatch(hostList.wakeonlanHost(ownProps._id))
    })
)(ViewPopup);

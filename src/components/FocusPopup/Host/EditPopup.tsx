import React from "react";
import { connect } from "react-redux";
import { ApplicationState, Id } from "../../../store";
import * as host from "../../../store/Host";
import * as focusPopup from "../../../store/FocusPopup";
import { form, textInput, textareaInput, selectInput, scanForm } from "../FormGenerators";
import { createHost, editHost } from "../../../store/HostList";
import Decoration from "../../Decoration";


class EditPopup extends React.Component<Props & Methods, State> {
    constructor(props: Props & Methods) {
        super(props);
        this.state = {
            isPowerControlEnabled: this.props.powerControlLevel && this.props.powerControlLevel !== 0,
        }
    }

    
    /** States, if the popup is editing an existing host */
    get isInEditMode() {
        return this.props._id;
    }

    /** States, if the popup is creating a new host */
    get isInCreateMode() {
        return !this.isInEditMode;
    }

    render() {
        return form("hostEditForm", [
            textInput("Hostname:", "name", this.props.name),
            textInput("SSH Address:", "address", this.props.address),
            textInput("SSH Port:", "sshPort", this.props.sshPort),
            selectInput("Power Control Level:", "powerControlLevel", [
                { 
                    selected: this.props.powerControlLevel === 0, 
                    displayName: "None (Always on)", 
                    value: 0,
                    onClick: () => this.setState({ isPowerControlEnabled: false }),
                },
                { 
                    selected: this.props.powerControlLevel === 1, 
                    displayName: "Task Scheduler", 
                    value: 1,
                    onClick: () => this.setState({ isPowerControlEnabled: true }),
                },
                { 
                    selected: this.props.powerControlLevel === 2, 
                    displayName: "User Queue", 
                    value: 2,
                    onClick: () => this.setState({ isPowerControlEnabled: true }),
                }
            ]),
            ...this.state.isPowerControlEnabled ? [
                textInput("WOL MAC-Address:", "wolMacAddress", this.props.wolMacAddress),
                textInput("WOL IP-Address:", "wolIpAddress", this.props.wolIpAddress),
                textInput("SSH Poweroff Command", "sshPoweroffCommand", this.props.sshPoweroffCommand),
            ] : [],
            textareaInput("Description:", "description", this.props.description),
            ...this.props.isLoading ? [ <Decoration type="LDS_ELLIPSIS_LOADER" />] : [ 
                <input type="button" className="interface-button button-done" onClick={
                    () => this.props.save(scanForm("hostEditForm"))
                }/> 
            ],
        ]);
    }
}



type Props = (focusPopup.State & any) | (focusPopup.State & host.State)
interface State {
    isPowerControlEnabled: boolean;
}
interface Methods {
    save: (state: host.State) => void;
};
export default connect(
    (state: ApplicationState, ownProps: {_id? : Id}): Props => ({
        ...ownProps._id ? state.hostList[ownProps._id] : {},
        ...state.focusPopup
    }),
    (dispatch: Function, ownProps: {_id? : Id}): Methods => ({
        save: (newState) => dispatch((ownProps._id ? editHost : createHost)({...newState, ...ownProps})),
    })
)(EditPopup);

import * as React from "react";
import * as focusPopup from "../../store/FocusPopup";
import { connect } from "react-redux";
import { ApplicationState } from "../../store";
import Popup from "reactjs-popup";
import ViewPopup from "./Host/ViewPopup";
import EditPopup from "./Host/EditPopup";


interface Methods {
    onClose: () => void;
}

type Props = focusPopup.State

class FocusPopup extends React.Component<Props & Methods> {
    private generateBody(): JSX.Element {
        switch (this.props.mode) {
            case "HOST_EDIT"  : return <EditPopup _id={ this.props.target }/>;
            case "HOST_VIEW"  : return <ViewPopup _id={ this.props.target }/>;
            case "HOST_CREATE": return <EditPopup />
            default           : return <div></div>;
        }
    }

    render() {
        return (
            <Popup
                open = { this.props.mode !== "NONE" }
                closeOnDocumentClick
                closeOnEscape
                children = { this.generateBody() }
                onClose = { this.props.onClose }
            />
        );
    }
}


export default connect(
    (state: ApplicationState): Props => ({
        ...state.focusPopup
    }),
    (dispatch: Function): Methods => ({
        onClose: () => dispatch(focusPopup.close())
    })
)(FocusPopup);
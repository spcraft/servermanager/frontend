import React from "react";
import Datetime from "react-datetime";
import "./react-datetime.css";
import { Moment } from "moment";
/** A template for key-value pairs  */
const inputTemplate = (key: JSX.Element | string, value: JSX.Element | string): JSX.Element => (
    <tr>
        <td>
            { key }
        </td>
        <td>
            { value }
        </td>
    </tr>
);


/** Generates a text input */
export const textInput = (displayName: string, propName: string, value?: string | number): JSX.Element => (
    inputTemplate(
        displayName,
        <input 
            type="text" 
            name={ propName }
            defaultValue={ typeof value === "number" ? value.toString() : value }
        />
    )
);

/** Generates a textarea input */
export const textareaInput = (displayName: string, propName: string, value?: string): JSX.Element => (
    inputTemplate(
        displayName,
        <textarea name={ propName } cols={ 30 } rows={ 10 } defaultValue = { value } />
    )
);

/** Generates an option input */
export const selectInput = (displayName: string, 
                            propName   : string, 
                            options     : {
                                displayName: string,
                                value?     : string | number, 
                                onClick?   : (event: any) => void
                                selected?  : boolean
                            }[]): JSX.Element => (
    inputTemplate(
        displayName,
        <select name={ propName }>{
            options.map(value => (
                <option 
                    selected={ value.selected ? true : false }
                    onClick={ value.onClick } 
                    value={ value.value }
                >{ value.displayName }</option>
            ))
        }</select>
    )
);

/** Generates a datetime input */
export const datetimeInput = (displayName   : string, 
                              defaultValue? : Date,
                              onDateChange? : (date: string | Moment) => void): JSX.Element => (
    inputTemplate(
        displayName,
        <Datetime 
            defaultValue={ defaultValue }
            onChange={ onDateChange }
            className="___DateTimeInput___"
        />
    )
);

export const checkboxInput = (displayName   : string,
                              propName      : string,
                              defaultChecked: boolean,
                              onChange      : (val: boolean) => void): JSX.Element => (
    inputTemplate(
        displayName,
        <input 
            type="checkbox" 
            name={ propName } 
            defaultChecked={ defaultChecked }
            onChange={ (event) => onChange(event.target.checked) }
        />
    )
)

/** Generates a form */
export const form = (formId: string, parts: JSX.Element[]): JSX.Element => (
    <form id={ formId }><table><tbody> { parts } </tbody></table></form>
);

/** Scans the form, returning an object */
export const scanForm = (formId: string): any => {
    const formObject: any = {};
    const scanInputsByType = (inputType: string): void => (
        document.querySelectorAll(`#${formId} ${inputType}`).forEach((element: any) => {
            formObject[element.name] = element.value;
        })
    );

    /* ---- SCANNING DEFAULT INPUTS ---- */
    scanInputsByType("input");
    scanInputsByType("textarea");
    scanInputsByType("select");

    return formObject;
}
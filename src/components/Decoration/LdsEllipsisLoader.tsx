import "./LdsEllipsisLoader.sass";
import * as React from 'react'

export default () => (
    <div className="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
)



import * as React from "react";
import LdsEllipsisLoader from "./LdsEllipsisLoader";

export interface Props {
    type: "LDS_ELLIPSIS_LOADER"
}

export default class Decoration extends React.Component<Props> {

    render() {
        switch(this.props.type) {
            case "LDS_ELLIPSIS_LOADER":
                return <LdsEllipsisLoader />
        }
    }
}

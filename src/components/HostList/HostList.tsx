import * as React from "react";
import Host from "../Host"
import * as hostList from "../../store/HostList";
import { connect } from "react-redux";
import { ApplicationState } from "../../store";
import * as focusPopup from "../../store/FocusPopup";

interface Methods {
    addHost: () => void
}

type Props = hostList.State;


class HostList extends React.Component<Props & Methods> {
    render() {
        return ([
            <h3>
                Host List
            </h3>,
            <input 
                type="button"
                className="interface-button button-add"
                onClick={ this.props.addHost }
            />,
            <div className="HostList">
                {
                    Object.keys(this.props).filter((key) => ("_id" in this.props[key])).map(
                        (hostId: string, index: number): JSX.Element => (
                            <Host key = { index } _id = { hostId }/>
                        )
                    )
                }
            </div>
        ]);
    }
}


export default connect(
    (state: ApplicationState): Props => ({ ...state.hostList }),
    (dispatch: Function): Methods => ({ 
        addHost: () => {
            dispatch(focusPopup.setMode("HOST_CREATE"));
        }
    }),
)(HostList);
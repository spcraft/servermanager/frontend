import * as React from "react";
import * as host from "../../store/Host";
import { connect } from "react-redux";
import store, { ApplicationState } from "../../store";
import Popup from "reactjs-popup";
import * as focusPopup from "../../store/FocusPopup";

interface PropMethods {

}

type IdObject = { _id: string }
type Props = host.State & PropMethods & IdObject;


class Host extends React.Component<Props> {

    focusSelf() {
        store.dispatch(focusPopup.setTarget(this.props._id));
        store.dispatch(focusPopup.setMode("HOST_VIEW"));
    }

    render() {
        const hostStatus: JSX.Element = (
            <div className="Host" onClick = { this.focusSelf.bind(this) }>
                <div className={ `status ${ this.props.isOnline ? "status-online" : "status-offline" }` }>
                    <img alt=""/>
                </div>
                { this.props.name }
            </div>
        );

        const hostInfo: JSX.Element = (
            <table><tbody>
                <tr>
                    <td> Hostname: </td>
                    <td> { this.props.name } </td>
                </tr>
                <tr>
                    <td> Address: </td>
                    <td> { this.props.address } </td>
                </tr>
                <tr>
                    <td> SSH Port: </td>
                    <td> { this.props.sshPort } </td>
                </tr>
                <tr>
                    <td> Status: </td>
                    <td> { this.props.isOnline ? "Online" : "Offline" } </td>
                </tr>
            </tbody></table>
        );

        return (
            <Popup 
                trigger  = { hostStatus }
                children = { hostInfo }
                position = "right top"
                on       = "hover"
            />
        )
    }
}

export default connect(
    (state: ApplicationState, ownProps: IdObject): host.State => ({ ...state.hostList[ownProps._id] }),
    (dispatch: Function, ownProps: IdObject): PropMethods => ({  }),
)(Host);
/**
 * A response, which is expected to be returned by the doJsonRequest
 */
export interface JsonResponse<T> {
    statusCode  : number,
    statusString: string,
    body        : T
}


/**
 * Performs a json request to the api, awaiting a json response
 * @param route to which the request is sent
 * @param method the method of the request
 * @param body the body of the request
 * @returns a promise to return a pair of a Status Code, a Status String, and a JSON response
 */
export default function doJsonRequest<T>(route: string,
                                         method: "GET" | "POST" | "PUT" | "DELETE",
                                         body?: any) : Promise<JsonResponse<T>> {
    /* ---- HEADERS SETUP ---- */
    let headers: any = {};
    headers["Accept"] = "application/json";
    if(body)
        headers["Content-Type"] = "application/json";

    /* ---- REQUEST MAKING ---- */
    return fetch(
        route,
        {
            credentials: "same-origin",
            method: method,
            headers: headers,
            body: JSON.stringify(body)
        }
    ).then(function(response: Response) {
        /* ---- RESPONSE PROCESSING ---- */
        return Promise.all([
            response.status,
            response.statusText,
            response.json(),
            !response.ok && !response.redirected
        ]);
    }).then(function([statusCode, statusString, body, fail]: [number, string, any, boolean]) {
        const result: JsonResponse<T> = {
            statusCode: statusCode,
            statusString: statusString,
            body: body
        };
        if(fail) return Promise.reject(result);
        return result;
    });
}

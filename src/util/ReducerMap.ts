import { AnyAction } from "redux";

export type ReducerMap<S extends any, A extends AnyAction> = {
    [key: string] : ((state: S, action: A) => S) | ReducerMap<S, A>
}
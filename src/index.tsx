import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import App from './components/App'
import './styles/style.sass'
import { Provider } from "react-redux"
import store from "./store";
import { fetchProfile } from "./store/Profile"
import { fetchHostList, fetchHost } from './store/HostList';

/* ---- LAUNCHING ON-START PROCESSES ---- */
(function loadHostList() {
    // LOADS A LIST OF ALL HOSTS, AS WELL AS THEIR CURRENT STATES
    store.dispatch(fetchHostList());
})();

/* ---- LAUNCHING ITERATING PROCESSES ---- */
(function validateLogin() {
    // VALIDATES LOGIN EVERY 20 SECONDS
    store.dispatch(fetchProfile());
    setTimeout(validateLogin, 20000);
})();

(function reloadHosts() {
    // RELOADS HOST STATES EVERY 10 SECONDS
    for(let hostId in store.getState().hostList)
        store.dispatch(fetchHost(hostId));
    setTimeout(reloadHosts, 10000);
})();



/* ---- RENDERING THE APP ---- */
ReactDOM.render(
    <Provider store={ store as any }><App /></Provider>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


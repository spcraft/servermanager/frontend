import typeToReducer from "type-to-reducer";
import { AnyAction, Reducer } from "redux";
import doJsonRequest from "../util/doJsonRequest";

/* ---- STATE AND ACTION DECLARATIONS ---- */
export interface State {
    isLoggedIn : boolean;

    uid        : string;
    cn         : string;
    mail       : string;
    loginShell : string;
};

const initialState: State = {
    isLoggedIn : true,
    uid        : "%username%",
    cn         : "%full_name%",
    mail       : "%email%",
    loginShell : "%login_shell%"
}

export type Action = AnyAction;


/* ---- REDUCER DECLARATION ---- */
export const reducer: Reducer<State, Action> = typeToReducer({
    PROFILE_FETCH: {
        PENDING    : (state: State, action: Action): State => (state),
        FULFILLED  : (state: State, action: Action): State => ({ 
            ...state, ...action.payload.body, isLoggedIn: true
        }),
        REJECTED   : (state: State, action: Action): State => {
            window.location.replace(
                "https://auth.spcraft.ga?application=manager" +
                "&callbackUri=" +
                window.location.protocol +
                "//" +
                window.location.hostname +
                ":" +
                window.location.port +
                "/login"
            );
            return ({
                ...state, isLoggedIn: false
            });
        }
    }
}, initialState);


/* ---- ACTION DECLARATION ---- */
export const fetchProfile = () => ({ 
    type: "PROFILE_FETCH", 
    payload: doJsonRequest("/user", "GET")
});
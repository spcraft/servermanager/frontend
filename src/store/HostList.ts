import typeToReducer from "type-to-reducer";
import { Action as _Action, Reducer } from "redux";
import doJsonRequest, { JsonResponse } from "../util/doJsonRequest";
import * as Host from "./Host";
import store from ".";
import * as UserQueue from "../store/UserQueue";

/* ---- STATE AND ACTION DECLARATIONS ---- */
export type State = {
    [hostId: string]: Host.State;
};

export const initialState: State = { }

export type Action<T = any> = _Action & { payload: T };



/* ---- REDUCER DECLARATION ---- */
export const reducer: Reducer<State, Action<any>> = typeToReducer({
    HOST_LIST_FETCH: {
        FULFILLED: (_state: State, action: Action<JsonResponse<Host.State[]>>): State => {
            const state: State = {};
            for(let host of action.payload.body)
                state[host._id] = host;
            return state;
        },
        REJECTED : (): State => ({ }),
    },
    HOST_FETCH     : {
        FULFILLED: (state: State, action: Action<JsonResponse<Host.State>>): State => ({
                ...state, [action.payload.body._id]: action.payload.body
        })
    }
}, initialState);



/* ---- ACTION DECLARATION ---- */
export const fetchHostList = () => ({
    type: "HOST_LIST_FETCH",
    payload: doJsonRequest<Host.State[]>(
        "/host?responseType=host", 
        "GET"
    ).then(res => {
        const promises: Promise<any>[] = [ Promise.resolve(res) ];
        res.body.forEach(host => {
            if(host.powerControlLevel < 2) return;
            promises.push(doJsonRequest<UserQueue.State>(
                `/host/${ host._id }/queue`,
                "GET"
            ).then(resp => {
                resp.body.myActiveQueueTasks.forEach(t => {
                    t.startDate = new Date(t.startDate);
                    if(t.endDate)
                        t.endDate = new Date(t.endDate);
                });
                host.userQueue = resp.body;
            }));
        });
        return Promise.all(promises) as Promise<JsonResponse<Host.State | void>[]>;
    }).then(responses => responses[0])
});


export const fetchHost = (hostOrId: string | Host.State) => ({
    type   : "HOST_FETCH",
    payload: doJsonRequest<Host.State>(
        `/host/${ 
            typeof hostOrId === "string" ? 
            hostOrId : hostOrId._id 
        }`, 
        "GET"
    ).then(res => 
        res.body.powerControlLevel >= 2 ?
        doJsonRequest<UserQueue.State>(
            `/host/${ res.body._id }/queue`,
            "GET"
        ).then(resp => {
            resp.body.myActiveQueueTasks.forEach(t => {
                t.startDate = new Date(t.startDate);
                if(t.endDate)
                    t.endDate = new Date(t.endDate);
            });
            res.body.userQueue = resp.body;
            return res;
        }) : res
    )
});

export const editHost = (host: Host.State) => ({
    type   : "HOST_EDIT",
    payload: doJsonRequest(`/host/${ host._id }`, "PUT", host)
});

export const createHost = (host: Host.State) => ({
    type   : "HOST_CREATE",
    payload: doJsonRequest<Host.State>(
        "/host", 
        "POST", 
        host
    ).then((host) => {
        store.dispatch(fetchHostList());
        return host;
    })
})

export const deleteHost = (hostOrId: string | Host.State) => ({
    type   : "HOST_DELETE",
    payload: doJsonRequest<Host.State>(
        `/host/${ 
            typeof hostOrId === "string" ? 
            hostOrId : hostOrId._id 
        }`, 
        "DELETE"
    ).then(() => {
        store.dispatch(fetchHostList());
    })
});

export const wakeonlanHost = (hostOrId: string | Host.State) => ({
    type: "HOST_WAKEONLAN",
    payload: doJsonRequest<any>(
        `/host/${ 
            typeof hostOrId === "string" ? 
            hostOrId : hostOrId._id 
        }/wakeonlan`,
        "GET"
    )
});

import * as Task from "./Task";

/* ---- STATE AND ACTION DECLARATIONS ---- */
export interface State {
    templateId: string;
    myActiveQueueTasks: Task.State[];
    usersOnline: string[];
}

export const initialState: State = {
    templateId        : "%template_id%",
    myActiveQueueTasks: [],
    usersOnline       : []
}



/* ---- REDUCER DECLARATION ---- */
// There is no reducer



/* ---- ACTION DECLARATION ---- */
// There are no actions
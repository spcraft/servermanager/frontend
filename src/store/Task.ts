import { IdObject } from ".";

/* ---- STATE AND ACTION DECLARATIONS ---- */
export interface State extends IdObject {
    /* ---- GENERAL INFO ---- */
    status       : number;
    statusString : string;
    template     : string;
    user         : string;

    /* ---- DATES ---- */
    startDate    : Date;
    endDate?     : Date;
}

export const initialState: State = {
    _id         : "%_id%",
    status      : NaN,
    statusString: "%status_string%",
    template    : "%template%",
    user        : "%user%",
    startDate   : new Date(),
    endDate     : new Date(),
}



/* ---- REDUCER DECLARATION ---- */
// There is no reducer



/* ---- ACTION DECLARATION ---- */
// There are no actions
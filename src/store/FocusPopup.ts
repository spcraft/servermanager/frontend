import typeToReducer from "type-to-reducer";
import { AnyAction, Reducer } from "redux";
import { Id } from ".";

/* ---- STATE AND ACTION DECLARATIONS ---- */
export type FocusMode = 
    | "NONE"
    | "HOST_VIEW"
    | "HOST_EDIT"
    | "HOST_CREATE";

export type FocusTarget = Id;

export interface State {
    /* ---- BASE COMPONENT STATE ---- */
    target       : FocusTarget;
    mode         : FocusMode
    
    /* ---- VIEW/EDIT COMPONENT STATE ---- */
    isLoading    : boolean;
};


export const initialState: State = { 
    target       : "",
    mode         : "NONE",

    isLoading    : false,
}

export type Action = AnyAction;

/* ---- REDUCER DECLARATION ---- */
export const reducer: Reducer<State, Action> = typeToReducer({
    FOCUS_POPUP: {
        TARGET_SET: (state: State, action: Action & { payload: FocusTarget }): State => ({
            // SETS THE TARGET OF THE POPUP
            ...state, target: action.payload
        }),
        MODE_SET: (state: State, action: Action & { payload: FocusMode }): State => ({
            // SETS THE MODE OF THE POPUP
            ...state, mode: action.payload
        }),
    },
    HOST_EDIT: {
        PENDING: (state: State, action: Action): State => ({
            ...state, isLoading: true
        }),
        FULFILLED: (state: State, action: Action): State => {
            alert("Host modified successfully");
            return ({
                ...state, isLoading: false, mode: "HOST_VIEW"
            })
        },
        REJECTED: (state: State, action: Action): State => {
            alert("Failed to edit host: " + action.payload.body.content);
            return ({
                ...state, isLoading: false
            })
        },
    },
    HOST_CREATE: {
        PENDING: (state: State, action: Action): State => ({
            ...state, isLoading: true
        }),
        FULFILLED: (state: State, action: Action): State => {
            alert("Successfully created host");
            return ({
                ...state, isLoading: false, target: action.payload.body._id, mode: "HOST_VIEW"
            });
        },
        REJECTED: (state: State, action: Action): State => {
            alert("Failed to create host: " + action.payload.body.content);
            return ({
                ...state, isLoading: false,
            });
        },
    },
    HOST_DELETE: {
        PENDING: (state: State, action: Action): State => ({
            ...state, isLoading: true
        }),
        FULFILLED: (state: State, action: Action): State => {
            alert("Host deleted successfully");
            return ({
                ...state, isLoading: false, mode: "NONE"
            });
        },
        REJECTED: (state: State, action: Action): State => {
            alert("Failed to delete host: " + action.payload.body.content);
            return ({
                ...state, isLoading: false
            });
        },
    },
    HOST_WAKEONLAN : {
        PENDING  : (state: State, action: Action): State => ({
            ...state, isLoading: true,
        }),
        FULFILLED: (state: State, action: Action): State => {
            alert(action.payload.body.content);
            return {...state, isLoading: false};
        },
        REJECTED : (state: State, action: Action): State => {
            alert("Failed to wakeonlan: " + action.payload.body.message);
            return {...state, isLoading: false};
        },
    },
}, initialState);



/* ---- ACTION DECLARATION ---- */
export const setTarget = (target: FocusTarget): Action => ({
    type: "FOCUS_POPUP_TARGET_SET",
    payload: target
});

export const setMode = (mode: FocusMode): Action => ({
    type: "FOCUS_POPUP_MODE_SET",
    payload: mode
});

export const close = (): Action => setMode("NONE");

export const startEdit = (): Action => setMode("HOST_EDIT");

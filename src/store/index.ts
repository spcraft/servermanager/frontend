import * as Profile from "./Profile";
import * as HostList from "./HostList";
import { combineReducers, createStore, applyMiddleware } from "redux";
import promise from "redux-promise-middleware";
import * as FocusPopup from "./FocusPopup";
// import logger from "redux-logger";


const rootReducer = combineReducers({
    profile : Profile.reducer,
    hostList: HostList.reducer,
    focusPopup: FocusPopup.reducer
});


export default createStore(
    rootReducer,
    applyMiddleware(
        promise,
        // logger,
    )
);

export type Id = string;
export interface IdObject {
    _id          : Id;
}


export type ApplicationState = ReturnType<typeof rootReducer>;
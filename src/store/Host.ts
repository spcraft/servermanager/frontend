import { IdObject } from ".";
import * as UserQueue from "./UserQueue";

/* ---- STATE AND ACTION DECLARATIONS ---- */
export interface State extends IdObject{
    /* ---- GENERAL INFO ---- */
    owner              : string;
    name               : string;
    description?       : string;
    isOnline           : boolean;

    /* ---- SSH INFO ---- */
    address            : string;
    sshPort            : number;

    /* ---- POWER CONTROL INFO ---- */
    powerControlLevel  : number & (0 | 1 | 2);
    wolMacAddress?     : string;
    wolIpAddress?      : string;
    sshPoweroffCommand?: string;
    userQueue?         : UserQueue.State;
}

export const initialState: State = {
    _id              : "%id%",
    owner            : "%owner_name%",
    name             : "%hostname%",
    address          : "%host_address%",
    sshPort          : NaN,
    isOnline         : false,
    powerControlLevel: 0,
}



/* ---- REDUCER DECLARATION ---- */
// There is no reducer



/* ---- ACTION DECLARATION ---- */
// There are no actions
